# Graph Solver

Quentin BERNARD

## Description du projet

Le projet “Graph Solver” projet vise à développer une application permettant de résoudre des problèmes d'ordonnancement simples. L'ordonnancement consiste à planifier l'exécution de différentes tâches dans le temps, en prenant en compte leurs contraintes de précédence et leurs durées.

Le contexte du projet repose sur l'exemple d'une enquête sur la consommation de glaces et sorbets, où neuf tâches ont été identifiées, telles que la réalisation des questionnaires, l'enquête téléphonique, l'enquête porte à porte, l'analyse des résultats, etc. Chaque tâche est caractérisée par sa durée et ses dépendances avec d'autres tâches.

Vidéo de présentation : lien

## Fonctionnalités

- Calcul de la date de début au plus tôt de chaque tâche.
- Détermination de la durée minimale du projet.
- Calcul de la date de début au plus tard de chaque tâche.
- Identification des marges pour chaque tâche, mettant en évidence les tâches critiques.
- Utilisation d'algorithmes spécifiques pour résoudre le problème d'ordonnancement.
- Chargement préalable du graphe du problème en mémoire.
- Stockage des résultats dans un fichier "results.txt".
- Créer un fichier “ABR.pdf” qui permet de visualiser l’arbre si l'on veut.


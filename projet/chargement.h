/*
 *  chargement.c
 *
 *  Created by Bernard Carré
 *
 *  Ce squelette de code ne fait que lire les données d'un fichier au format DIMACS du projet et les reafficher.
 *
 *  Vous en inspirer pour construire votre SD a partir des donnees ainsi obtenues
 *  en incrustant les lignes de code necessaires.
 *
 *  Un main de test est fourni, a parametrer par un fichier DIMACS :
 *  > gcc -o main chargement.c
 *  > main fichier-dimacs
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE_SIZE 256

/*
 * Extrait les donnees du fichier de nom file_name et les initilialise la structure 
 * graphe passé en paramètre.
 * Provoque une erreur si le fichier est inaccessible.
 */
void chargement(char* file_name, struct graphe* g);

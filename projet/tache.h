#include <stdio.h>
#include <stdlib.h>

struct tache{
    int numeroTache; //Numero de la tache
    int duree; //Durée de la tache
    struct liste_tache* preds; //Liste d'indices des prédécesseurs
    int date_plus_tot; //Date au plus tot de la tache
    int date_plus_tard; //Date au plus tard de la tache
    int nbPred; //Nombre de prédecesseurs
    int nbSucc; //Nombre de successeurs
};

struct liste_tache{
    int numeroTache; //Numero de la tache
    struct liste_tache* suivant; //Tache suivante de la liste de prédécesseur
};

#define NIL (struct liste_tache*)0

/*Fonction qui intialise la tache t en paramètre*/
void init_tache(struct tache* t, int num_noeud, int duree, int nb_noeuds);

/*Fonction qui ajouter à la tache t en paramètre un nouveau prédécesseur (indice idxPred)*/
void ajout_pred(struct tache* tache, int idxPred);

/*Fonction qui free la liste de prédécesseur de la structure tache*/
void free_liste_tache(struct liste_tache* lt);
#include "tache.h"
#include "graphe.h"
#include "chargement.h"

/*
 * Usage : 
 * - gcc *.c -Wall
 * - ./a.out ./path/to/file.txt
 * - ./a.out ./path/to/file.txt -g       (ou cette commaande pour afficher le graphique)
 */
int main(int argc, char *argv[]) {
    if(argc<2 || argc>3) {
        fprintf(stderr, "Usage: main fichier-dimacs\n");
        exit(1);
    }

    //CREATION GRAPHE
    struct graphe* g;
    g = malloc(sizeof(struct graphe));
    chargement(argv[1], g);

    //SORTED
    int* N;
    int* sorted;
    N = malloc(sizeof(int)*g->nb_noeuds);
    sorted = malloc(sizeof(int)*g->nb_noeuds); 

    procedure_niveau(*g, N);
    procedure_tri(N, sorted, g->nb_noeuds);

    //BELLMAN
    bellman_max(g, sorted);
    bellman_min(g, sorted);

    //RESULTATS
    FILE *f;
	f = fopen("result.txt", "w");
	if(f!=NULL){
        //Infos relatives au graphe
        fprintf(f, "\n=========================\n\n");

        fprintf(f, "Nombre de noeuds = %d\n", g->nb_noeuds);
        fprintf(f, "Durée minimal = %d\n",  g->taches[sorted[g->nb_noeuds-1]].date_plus_tot);
        fprintf(f, "Taches critiques = [");
        for(int i = 0; i<g->nb_noeuds; i++){
            if(g->taches[i].date_plus_tard - g->taches[i].date_plus_tot<=0){
                fprintf(f, "%d", i);
                if(i<g->nb_noeuds-1) fprintf(f, ", ");
            }
        }
        fprintf(f, "]\n");

        fprintf(f, "\n=========================\n\n");

        //Infos sur chacune des taches
        for(int i = 0; i<g->nb_noeuds; i++){
            fprintf(f, "\nTache %3d :\n- durée = %3d\n- date au plus tot = %3d\n- date au plus tard = %3d\n- marge = %3d\n", g->taches[i].numeroTache, g->taches[i].duree, g->taches[i].date_plus_tot, g->taches[i].date_plus_tard, (g->taches[i].date_plus_tard - g->taches[i].date_plus_tot));
        }
    }
    fclose(f);
    //OPTIONNELLE - AFFICHAGE DU GRAPHE AVEC LA COMMANDE DOT
    if(argc==3 && strcmp("-g", argv[2])==0 && g->nb_noeuds<=1000)
        visualise_graphe(g);

    //FREE LE GRAPH
    free_graphe(g);
    free(N);
    free(sorted);

    printf("fini\n");
    return(0);
}
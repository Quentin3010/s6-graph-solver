#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

/*
* Structure graphe
*/
struct graphe{
    int nb_noeuds; //Nombre de noeuds du graphe (source et puit compris)
    struct tache* taches; //Vecteurs de toutes les taches du graphe 
                          //=> Source = taches[0] / Tache 1 = taches[1] / ...
};

/*Affiche le graphe en affichant toutes les infos de chacune des taches (sert à débuguer)*/
void print_graphe(struct graphe* g);

/*Initalise le vecteur N en paramètre avec les taches trié par couche (issue du pdf)*/
void procedure_niveau(struct graphe G, int* N);

/*Initalise le vecteur N en paramètre avec les taches trié par couche (issue du pdf)*/
void procedure_tri(int* N, int* S, int n);

/*
* Fonction qui permet de déterminer les dates au plus tot de chaque tache grâce à l'algorithme 
* de bellman, avec un parcours du plus long chemin en profondeur
*/
void bellman_max(struct graphe* g, int* sorted);

/*
* Fonction qui permet de déterminer les dates au plus tard de chaque tache grâce à l'algorithme 
* de bellman 
*/
void bellman_min(struct graphe* g, int* sorted);

/*
* Fonction qui permet d'afficher le graphe visuellement grâce à la commande dot
* ./a.out file.txt -g       <= -g pour afficher le graphe
*/
void visualise_graphe(struct graphe* g);

/*Fonction pour free la structure graphe*/
void free_graphe(struct graphe* g);
#include "tache.h"
#include "graphe.h"
#include "chargement.h"

void chargement(char* file_name, struct graphe* g) { 
    char ligne[MAX_LINE_SIZE]; // pour le passage de ligne
    int nb_noeuds; // nombre de noeuds
    
    // Acces au fichier.
    FILE* fp = fopen(file_name,"r");
    if (fp==NULL) {
        fprintf(stderr, "%s non accessible\n", file_name);
        exit(1);
    }

    /**********************/
    /* Lecture du fichier */
    /**********************/
 
    // 'c nom du probleme' en ligne de commentaire, on passe ...
    fgets(ligne,MAX_LINE_SIZE,fp);

    // 'p nombre de noeuds'
    fgetc(fp);fgetc(fp); //on passe l'entete 'p '
    fscanf(fp,"%d",&nb_noeuds);
    nb_noeuds = nb_noeuds + 2;

    g->nb_noeuds = nb_noeuds;

    // Lecture des noeuds : lignes 'v num_noeud duree' 
    int num_noeud; 
    int duree;
    
    g->taches = malloc(sizeof(struct tache)*nb_noeuds);
    init_tache(&g->taches[0], 0, 0, nb_noeuds);
    init_tache(&g->taches[nb_noeuds-1], nb_noeuds-1, 0, nb_noeuds);
    for(num_noeud=1; num_noeud<=nb_noeuds-2; num_noeud++) {
        fgetc(fp);fgetc(fp); //on passe l'entete 'v '
        fscanf(fp,"%d",&num_noeud);
        fscanf(fp,"%d",&duree);
         
        //initialisation des taches
        init_tache(&g->taches[num_noeud], num_noeud, duree, nb_noeuds);
    }

    // Lecture des arcs : lignes 'a num_pred num_noeud'
	int num_pred; 
	int c;

	fgets(ligne,MAX_LINE_SIZE,fp); // passage a la ligne
	c=fgetc(fp);
	while(c=='a') {
		fscanf(fp, "%d", &num_pred);
		fscanf(fp, "%d", &num_noeud);

		fgets(ligne,MAX_LINE_SIZE,fp); // passage a la ligne
		c=fgetc(fp);
        ajout_pred(&g->taches[num_noeud], num_pred);
        g->taches[num_noeud].nbPred += 1;
        g->taches[num_pred].nbSucc += 1;
    }

    // Gestion du puit et de la source
    for(int i = 1; i<nb_noeuds-1; i++){
        if(g->taches[i].nbPred==0){
            ajout_pred(&g->taches[i], 0);
            g->taches[i].nbPred = 1;
            g->taches[0].nbSucc += 1;
        }
        if(g->taches[i].nbSucc==0){
            ajout_pred(&g->taches[nb_noeuds-1], i);
            g->taches[nb_noeuds-1].nbPred += 1;
            g->taches[i].nbSucc = 1;
        }
    }

    fclose(fp);
}

#include "tache.h"

/*Fonction qui intialise la tache t en paramètre*/
void init_tache(struct tache* t, int num_noeud, int duree, int nb_noeuds){
    t->duree = duree;
    t->numeroTache = num_noeud;

    t->date_plus_tot = -1;
    t->date_plus_tard = 100;

    t->nbPred = 0;
    t->nbSucc = 0;

    t->preds = NIL;
}

/*Fonction qui ajouter à la tache t en paramètre un nouveau prédécesseur (indice idxPred)*/
void ajout_pred(struct tache* tache, int idxPred){
    if(tache->nbPred==0){
        tache->preds = malloc(sizeof(struct liste_tache));
    }
    struct liste_tache* A;
    A = malloc(sizeof(struct liste_tache));
    A->suivant = tache->preds;
    A->numeroTache = idxPred;

    tache->preds = A;
}

/*Fonction qui sert à free la liste de prédécesseur de la structure tache*/
void free_liste_tache(struct liste_tache* lt){
    if(lt!=NIL){
        free_liste_tache(lt->suivant);
        free(lt);
    }
}
#include "tache.h"
#include "graphe.h"

/*Affiche le graphe en affichant toutes les infos de chacune des taches (sert à débuguer)*/
void print_graphe(struct graphe* g){
    printf("Print Graphe:\n");
    struct liste_tache* A;
    for(int i = 0; i<(g->nb_noeuds); i++){
        printf("Taches n°%d - (Durée = %d)\n", g->taches[i].numeroTache, g->taches[i].duree);
        A = g->taches[i].preds;
        for(int j = 0; j<g->taches[i].nbPred; j++){
            printf("- Taches n°%d\n", A->numeroTache);
            A = A->suivant;
        }
    }
    printf("\n");
}

/*Initalise le vecteur N en paramètre avec les taches trié par couche (issue du pdf)*/
void procedure_niveau(struct graphe G, int* N){
    int change = true;
    for(int i = 0; i<G.nb_noeuds; i++){
        N[i] = 0;
    }

    struct liste_tache* A;
    while(change){
        change = false;
        for(int i = 0; i<G.nb_noeuds; i++){
            //Pour tous les prédécesseur de g->taches[i]
            A = G.taches[i].preds;
            for(int j = 0; j<G.taches[i].nbPred; j++){
                
                if(N[i]<N[A->numeroTache]+1){
                    N[i] = N[A->numeroTache] + 1;
                    change = true;
                }
                A = A->suivant;
            }
        }   
    }
}

/*Initalise le vecteur N en paramètre avec les taches trié par couche (issue du pdf)*/
void procedure_tri(int* N, int* S, int n){
    int p = 0;
    int niv = 0;
    while(p<n){
        for(int i = 0; i<n; i++){
            if(N[i]==niv){
                S[p] = i;
                p++;
            }
        }
        niv++;
    }
}

/*
* Fonction qui permet de déterminer les dates au plus tot de chaque tache grâce à l'algorithme 
* de bellman, avec un parcours du plus long chemin en profondeur
*/
void bellman_max(struct graphe* g, int* sorted){
    //pere de chaque tache 
    int* pere = malloc(sizeof(int)*g->nb_noeuds);
    for(int i = 0; i<g->nb_noeuds; i++) 
        pere[i] = 0;

    int racine = sorted[0];

    g->taches[racine].date_plus_tot = 0;
    pere[racine] = racine;

    struct liste_tache* A;
    int x,y;
    for(int i = 1; i<g->nb_noeuds; i++){
        y = sorted[i]; 
        //Pour tous les prédécesseur de g->taches[i]
        A = g->taches[y].preds;
        for(int j = 0; j<g->taches[y].nbPred; j++){
            x = A->numeroTache;

            if( g->taches[x].date_plus_tot!=-1 && ( g->taches[x].date_plus_tot+g->taches[x].duree)> g->taches[y].date_plus_tot){
                g->taches[y].date_plus_tot =  g->taches[x].date_plus_tot + g->taches[x].duree;
                pere[y] = x;
            }
            A = A->suivant;
        }
    }

    for(int i = 0; i<g->nb_noeuds; i++){
        g->taches[i].date_plus_tard = g->taches[g->nb_noeuds-1].date_plus_tot;
    }

    free(pere);
}

/*
* Fonction qui permet de déterminer les dates au plus tard de chaque tache grâce à l'algorithme 
* de bellman 
*/
void bellman_min(struct graphe* g, int* sorted){
    struct liste_tache* A;
    for(int i = g->nb_noeuds-1; i>=0; i--){
        int y = sorted[i]; 
        //Pour tous les prédécesseur de g->taches[i]
        A = g->taches[i].preds;
        for(int j = 0; j<g->taches[i].nbPred; j++){
            int x = A->numeroTache;
            if(g->taches[x].date_plus_tard > g->taches[y].date_plus_tard - g->taches[x].duree){
                g->taches[x].date_plus_tard = g->taches[y].date_plus_tard - g->taches[x].duree;
            }
            A = A->suivant;
        }
    }
}

/*
* Fonction qui permet d'afficher le graphe visuellement grâce à la commande dot
* ./a.out file.txt -g       <= -g pour afficher le graphe
*/
void visualise_graphe(struct graphe* g){
	FILE *f;
	f = fopen("ABR.dot", "w");
	if(f!=NULL){
		fprintf(f, "digraph G { node [shape=circle]\n");

        struct liste_tache* A;
        for(int i = 0; i<g->nb_noeuds; i++){
            //Pour tous les prédécesseur de g->taches[i]
            A = g->taches[i].preds;
            for(int j = 0; j<g->taches[i].nbPred; j++){
                fprintf(f, "T%d -> T%d [label=\"%d\"];\n", A->numeroTache, i, g->taches[A->numeroTache].duree);
                A = A->suivant;
            }
        }
		
		fprintf(f, "}\n");
		fclose(f);
		
		system("dot -Tpdf ABR.dot -Grankdir=LR -o ABR.pdf");
		system("evince ABR.pdf &");
	}
}

/*Fonction pour free la structure graphe*/
void free_graphe(struct graphe* g){
    for(int i = 0; i<g->nb_noeuds; i++){
        free_liste_tache(g->taches[i].preds);
    }
    free(g->taches);
    free(g);
}